VIEW = 'view_status'
ADD = 'add_status'
EDIT = 'edit_status'
DELETE = 'delete_status'

NO_RECORD = 'No records found'
RECORD_ADD = 'Record added successfully'
RECORD_UPDATE = 'Record updated successfully'
RECORD_DELETE = 'Record deleted successfully'
RECORD_SAVE = 'Record saved successfully'

UNAUTHORIZED_TOKEN = 'Your token is unauthorized'
NOT_AUTHORIZED = 'You are not authorized to view admin settings'
INTERNAL_SERVER_ERROR = 'Something went wrong'
TOKEN_EMPTY = 'Token is empty'

OPERATIONS = {
    'state_master': 'state-master',
    'district_master': 'district-master',
    'organisation_master': 'organisation-master',
    'corporation_master': 'corporation-master',
    'user_master': 'user-master',
    'role_master': 'role-master',
    'place_master': 'place-master',
    'zone': 'zone'
}

GRPC_ERROR = 'The gRPC server has responded with status '

USER = 'user'
OPERATION = 'operation'

CORPORATION_ADDRESS = 1
ZONE_ADDRESS = 2
DEPOT_ADDRESS = 3
BUSSTATION_ADDRESS = 4
FRANCHISEE_ADDRESS = 5
COUNTER_ADDRESS = 6

USER_TYPES = {
    'ADMIN': 1,
    'FRANCHISEE': 2,
    'OPERATOR': 3,
    'CUSTOMER': 4
}

OTP_SEND = 'OTP has been sent to the given email and phone number'
OTP_RESEND = 'OTP has been resent successfully'
OTP_MATCH = 'OTP Matched Successfully'
OTP_SEND_PHONE = 'OTP has been sent successfully to your phone number'
OTP_SEND_EMAIL = 'OTP has been sent successfully to your email'
