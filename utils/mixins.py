from rest_framework.response import Response
from rest_framework import status
from .serializers import GrpcErrorSerializer
from . import constants


class ResponseViewMixin(object):
    """
    Mixin to handle micro service channel, and errors
    """

    def otp_response(self, code='HTTP_200_OK', data=None):
        return Response(
            headers={'status': getattr(status, code)},
            status=getattr(status, code),
            data={
                'status': getattr(status, code),
                'data': data
            },
            content_type='application/json'
        )

    def otp_error_response(self, code='HTTP_500_INTERNAL_SERVER_ERROR', data=None):
        mobile_error = data[0] if isinstance(data[0], str) else data[0].get('error_message', None)
        return Response(
            headers={'status': getattr(status, code)},
            status=getattr(status, code),
            data={
                'status': getattr(status, code),
                'error': data,
                'mobile_error': mobile_error
            },
            content_type='application/json'
        )

    def otp_exception_response(self, e_code):
        msg = "The grpc server responded with " + e_code.lower() + " status"

        return self.otp_error_response('HTTP_500_INTERNAL_SERVER_ERROR', msg)

    def otp_validation_error_response(self, response):

        # check whether there is message/error_message key in response
        response_fields_set = set(response.DESCRIPTOR.fields_by_name.keys())
        error_keys = {'error_message', 'message'}
        error_key_exists = response_fields_set.intersection(error_keys)

        if 'validations' in response.DESCRIPTOR.fields_by_name and response.validations:
            validation_serializer = GrpcErrorSerializer(response.validations, many=True)
            if 'already exists' in validation_serializer.data[0]['error_message']:
                validation_serializer.data[0]['field'] = '__all__'

            return self.otp_error_response('HTTP_400_BAD_REQUEST', validation_serializer.data)

        elif error_key_exists:

            return self.otp_error_response('HTTP_400_BAD_REQUEST', [getattr(response, error_key_exists.pop())])

        return self.otp_error_response('HTTP_500_INTERNAL_SERVER_ERROR', [constants.INTERNAL_SERVER_ERROR, ])

    def send_response(self, response, message=None):
        if response.status:
            id = response.id if 'id' in response.DESCRIPTOR.fields_by_name else None
            phone_number = response.phone_number if 'phone_number' in response.DESCRIPTOR.fields_by_name else None
            is_phone_number_changed = response.is_phone_number_changed \
                if 'is_phone_number_changed' in response.DESCRIPTOR.fields_by_name else None
            response = {'message': message}
            if id:
                response['id'] = id
            if phone_number:
                response['phone_number'] = phone_number
            if is_phone_number_changed:
                response['is_phone_number_changed'] = is_phone_number_changed
            return self.otp_response('HTTP_200_OK', data=response)

        return self.otp_validation_error_response(response)

    def process_response_data(self, data, serializer):
        if data.status:
            serialized_result = serializer(data)

            return self.otp_response('HTTP_200_OK', data=serialized_result.data)

        return self.otp_validation_error_response(data)
