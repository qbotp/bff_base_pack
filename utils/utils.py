import grpc

from datetime import datetime
from calendar import timegm
from . import constants
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import exception_handler
from rest_framework_jwt.settings import api_settings

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def get_message(grpc_status):
    """
    To process grpc error message
    :param grpc_status: gRPC error code
    :return: String
    """
    message = [constants.GRPC_ERROR + str(grpc_status).lower()]

    return message


def otp_exception_handler(exc, context):
    """
    Custom execption handler
    Call REST framework's default exception handler first, to get standard error response
    """
    response = Response()
    response.data = {}
    if isinstance(exc, ValueError) or isinstance(exc, KeyError):
        response['Status'] = status.HTTP_400_BAD_REQUEST  # Set status in response header
        response.data['status'] = status.HTTP_400_BAD_REQUEST
        response.data['error'] = ['Invalid request']
        response.data['mobile_error'] = 'Invalid request'

    elif isinstance(exc, grpc.RpcError):
        response['Status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
        response.data['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
        response.data['error'] = get_message(exc.code().name)
        error_message = str(get_message(exc.code().name))
        response.data['mobile_error'] = str.strip(error_message, '[\']')
    else:
        response = exception_handler(exc, context)
        if response is not None:
            response['Status'] = response.status_code
            response.data['status'] = response.status_code
            response.data['error'] = [str(exc)]

    return response


def jwt_payload_handler(user):
    """
    jwt payload handler
    Args:
        user: user object

    Returns:
        payload - payload data
    """
    payload = {
        'user_id': str(user.pk),
        'username': user.username,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'user_type': user.user_type
    }

    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload


class K:
    def __init__(self, label=None, **kwargs):
        assert(len(kwargs) == 1)
        for k, v in kwargs.items():
            self.id = k
            self.v = v
        self.label = label or self.id


class Konstants:
    def __init__(self, *args):
        self.klist = args
        for k in self.klist:
            setattr(self, k.id, k.v)

    def choices(self):
        return [(k.v, k.label) for k in self.klist]

    def display(self, k):
        for ks in self.klist:
            if k == ks.v:
                return ks.label
        return ""

    def __getitem__(self, k):
        return self.display(k)

AUTHENTICATION_MEDIA = Konstants(
    K(facebook='facebook', label='Facebook'),
    K(google='google', label='Google'),
    K(twitter='twitter', label='Twitter'),
    K(instagram='instagram', label='Instagram'),
    K(linkedin='linkedin', label='LinkedIn'),
)

USER_TYPES = Konstants(
    K(admin=1, label='Admin'),
    K(franchisee=2, label='Franchisee'),
    K(counter=3, label='Counter'),
    K(customer=4, label='Customer')
)

PAYMENT_STATUS = Konstants(
    K(INI=1, label='Initialized'),
    K(GPA=2, label='Gateway Payment Aborted'),
    K(GPC=3, label='Gateway Payment Confirmed and cannot create PNR'),
    K(CNF=4, label='Gateway payment Confirmed and created PNR')
)
