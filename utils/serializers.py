from rest_framework import serializers

class GrpcErrorSerializer(serializers.Serializer):
    field = serializers.CharField(read_only=True)
    error_message = serializers.CharField(read_only=True)